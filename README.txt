UMass Global Branding
=====================

UMass Global Branding module swaps in a global header and footer from external markup, css, and js resources. This applies to all pages except Drupal admin pages.

This module takes no input, loads external static files, and has zero configuration.

See umass_global_branding.module file for location of external static resources.

Clear the Drupal site cache to reload and refresh on demand.

Cached global branding resources will expire and reload every 24 hours.

Include the following CSS for any custom pages which should not have branding.

    #umass--global--header, #umass--global--footer {
      display: none;
    }

Authors
=======

David Ruderman <david.ruderman@umass.edu>
